This is a simple view that displays an image if field_featured_image is a field on a content type.

Place this view block in the "Header" region of ashlar to display a header image that populates from the above field based on the node ID. 
